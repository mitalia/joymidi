#ifndef JOYSTICK_HPP_INCLUDED
#define JOYSTICK_HPP_INCLUDED

#include <linux/joystick.h>
#include <vector>
#include <string>

namespace IO
{
    class Joystick
    {
        private:
        //Path of the device
        std::string path;
        //File descriptor of the device
        int fileDescriptor;
        //True if fileDescriptor must be closed on destruction
        bool closeOnDestruction;

        //Features retrieved via IOCTL
        //Joystick driver version
        unsigned int driverVersion;
        //Axes' number
        unsigned char axesNumber;
        //Buttons' number
        unsigned char buttonsNumber;
        //Device name
        std::string deviceName;

        //Note: the following values are updated only when a new event is explicitly retrieved
        //Axes' last values
        std::vector<__s16> lastAxesValues;
        //Buttons' last values
        std::vector<bool> lastButtonsValues;
        //Last event retrieved
        js_event lastEvent;

        //Automatic transformations applied to lastAxesValues and lastButtonsValues
        //If an element in this vector is true, the value of the corresponding axis value will be inverted (-32768=>32767; 32767=>-32768)
        std::vector<bool> invertedAxes;
        //If an element in this vector is true, the value of the corresponding button value will be inverted (true=>false; false=>true)
        std::vector<bool> invertedButtons;

        //Init the class
        void init();

        //Sets the file descriptor in blocking/nonblocking mode; returns the previous state
        bool setFileDescriptorMode(bool Block);
        public:
        //Constructors
        //From a file descriptor
        Joystick(int FileDescriptor, bool CloseOnDestruction);
        //From a device name
        Joystick(const std::string & Path);

        //Destructor
        ~Joystick();

        //Getters
        //Path
        const std::string & GetPath() { return path; };
        //File descriptor
        int GetFileDescriptor() { return fileDescriptor; };
        //Driver version
        unsigned int GetDriverVersion() { return driverVersion; };
        //Axes' number
        unsigned int GetAxesNumber() { return (unsigned int)axesNumber; };
        //Buttons' number
        unsigned int GetButtonsNumber() { return (unsigned int)buttonsNumber; };
        //Device name
        const std::string & GetDeviceName() { return deviceName; };

        //Axes' last values
        const std::vector<__s16> & GetLastAxesValues() { return lastAxesValues; };
        //Buttons' last values
        const std::vector<bool> & GetLastButtonsValues() { return lastButtonsValues; };

        //Axes' invertion settings
        std::vector<bool> & GetInvertedAxes() { return invertedAxes; };
        //Buttons' invertion settings
        std::vector<bool> & GetInvertedButtons() { return invertedButtons; };

        //Last event retrieved
        const js_event & GetLastEvent() { return lastEvent; };

        //Retrieves an event updating the values stored in the class; to get such message use GetLastEvent.
        //Returns false if no event was retrieved
        bool GetEvent(bool Block=true);
        //Updates the values of the class, emptying the joystick driver event buffer; returns false if no event was retrieved
        bool UpdateValues();

    };
}


#endif // JOYSTICK_HPP_INCLUDED
