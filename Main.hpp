#ifndef MAIN_HPP_INCLUDED
#define MAIN_HPP_INCLUDED

//Starts the monitoring of the joystick
void StartJoystickMonitoring(const char * JoystickDevice, unsigned int Channel);

//Opens a new MIDI sequencer
snd_seq_t * OpenMIDISequencer();

//Creates a new MIDI readable port
int CreateMIDIReadablePort(snd_seq_t *Sequencer, const char * PortName);

//Sends an event on the given MIDI sequencer
void SendMIDIEvent(snd_seq_t * Sequencer, snd_seq_event_t & Event);

//Flushes the MIDI events buffer
void FlushMIDIEventsBuffer(snd_seq_t * Sequencer);

#endif // MAIN_HPP_INCLUDED
