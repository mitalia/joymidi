//System headers
#include <sys/ioctl.h>
#include <linux/joystick.h>
#include <fcntl.h>

//Application headers
#include "Utils.hpp"
#include "Joystick.hpp"
#include "ErrnoError.hpp"

/** @brief UpdateValues
  *
  * @todo: document this function
  */
bool IO::Joystick::UpdateValues()
{
    //Continue to extract events until the buffer is empty
    bool ret=false;
    while(GetEvent(false))
        ret=true;
    return ret;
}

/** @brief setFileDescriptorMode
  *
  * @todo: document this function
  */
bool IO::Joystick::setFileDescriptorMode(bool Block)
{
    int fdFlags;
    bool ret;
    fdFlags = fcntl(fileDescriptor, F_GETFL);
    if(fdFlags==-1)
        throw Utils::ErrnoError("Cannot get the flags for the joystick file descriptor.");
    ret=!(fdFlags&O_NONBLOCK);
    if(Block)
        fdFlags &= ~O_NONBLOCK;
    else
        fdFlags |= O_NONBLOCK;
    if(fcntl(fileDescriptor, F_SETFL, fdFlags)==-1)
        throw Utils::ErrnoError("Cannot set the flags for the joystick file descriptor.");
    return ret;
}

/** @brief GetEvent
  *
  * @todo: document this function
  */
bool IO::Joystick::GetEvent(bool Block)
{
    //Temporary event
    js_event newEvent;
    //Return value
    bool ret=false;
    //Previous block mode
    bool previousBlockMode;
    //Set the file descriptor to the desidered mode
    previousBlockMode=setFileDescriptorMode(Block);
    try
    {
        //Read the new event
        switch(read(fileDescriptor,&newEvent,sizeof(newEvent)))
        {
            case sizeof(newEvent):
                //Ok, we've just read an event
                ret=true;
                //Copy it in lastEvent
                lastEvent=newEvent;
                break;
            case -1:
                //We have a reading error
                if(errno!=EAGAIN) //If it's EAGAIN it's normal, there are no events
                    throw Utils::ErrnoError("Error retrieving a joystick event.");
                break;
            default:
                throw std::runtime_error("Error retrieving a joystick event; read returned an unexpected number of bytes.");
        }
        //If we retrieved something, update the stored values
        if(ret)
        {
            switch(lastEvent.type & ~JS_EVENT_INIT)
            {
                case JS_EVENT_AXIS:
                    //Always be careful...
                    if(lastEvent.number>=axesNumber)
                        throw std::runtime_error("The driver reported an event for a nonexistent axis.");
                    //TODO: check if the range of lastEvent.value is -32768 to 32767 or -32767 to 32767; here assuming the second one
                    lastAxesValues[lastEvent.number]=invertedAxes[lastEvent.number]?-lastEvent.value:lastEvent.value;
                    break;
                case JS_EVENT_BUTTON:
                    //Always be careful...
                    if(lastEvent.number>=buttonsNumber)
                        throw std::runtime_error("The driver reported an event for a nonexistent button.");
                    lastButtonsValues[lastEvent.number]=invertedButtons[lastEvent.number]?lastEvent.value==0:lastEvent.value!=0;
                    break;
                //Unknown events are ignored
            }
        }
        //Restore the previous block mode
        setFileDescriptorMode(previousBlockMode);
    }
    catch(...)
    {
        //Restore the previous block mode
        setFileDescriptorMode(previousBlockMode);
        //Rethrow
        throw;
    }
    return ret;
}

/** @brief ~Joystick
  *
  * @todo: document this function
  */
 IO::Joystick::~Joystick()
{
    if(closeOnDestruction && fileDescriptor != -1)
        close(fileDescriptor);
}

/** @brief Joystick
  *
  * @todo: document this function
  */
 IO::Joystick::Joystick(const std::string & Path) :
    path(Path),
    fileDescriptor(-1),
    closeOnDestruction(true)
{
    init();
}

/** @brief Joystick
  *
  * @todo: document this function
  */
 IO::Joystick::Joystick(int FileDescriptor, bool CloseOnDestruction) :
    fileDescriptor(FileDescriptor),
    closeOnDestruction(CloseOnDestruction)
{
    init();
}

/** @brief init
  *
  * @todo: document this function
  */
void IO::Joystick::init()
{
    try
    {
        //If necessary, open the file
        if(fileDescriptor==-1)
        {
            //We don't have the magic ball
            if(path.size()==0)
                throw std::invalid_argument("You must supply a valid file descriptor or a valid path.");
            //Open the file
            fileDescriptor=open(path.c_str(),O_RDONLY);
            if(fileDescriptor==-1)
                throw Utils::ErrnoError("Cannot open the joystick device.");
        }
        //Retrieve the data from the file descriptor
        char tempDeviceName[256];
        ioctl(fileDescriptor, JSIOCGVERSION, &driverVersion);
        ioctl(fileDescriptor, JSIOCGAXES, &axesNumber);
        ioctl(fileDescriptor, JSIOCGBUTTONS, &buttonsNumber);
        ioctl(fileDescriptor, JSIOCGNAME(ARRSIZE(tempDeviceName)), tempDeviceName);
        deviceName=tempDeviceName;
        //Check the driver version
        if(driverVersion<0x010000)
            throw std::runtime_error("The joystick driver version is too old, this class cannot work with it.");
        //Size the vectors
        lastAxesValues.resize(axesNumber, 0);
        lastButtonsValues.resize(buttonsNumber, false);
        invertedAxes.resize(axesNumber, false);
        invertedButtons.resize(buttonsNumber, false);
    }
    catch(...)
    {
        if(closeOnDestruction && fileDescriptor != -1)
            close(fileDescriptor);
        throw;
    }
}
