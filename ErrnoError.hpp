#ifndef ERRNOERROR_HPP_INCLUDED
#define ERRNOERROR_HPP_INCLUDED

#include <stdexcept>
#include <string>
#include <cstring>
#include <errno.h>

namespace Utils
{
    //Represent an error with an errno or another error code translable to a string with a strerror/strerror_r-like function
    class ErrnoError : public std::runtime_error
    {
        public:
        //Possible translating functions
        typedef const char * (*strerrorFunc)(int ErrorNumber);
        typedef int (*strerror_rXSIFunc)(int ErrorNumber, char * Buffer, size_t BufferLength);
        typedef const char * (*strerror_rGNUFunc)(int ErrorNumber, char * Buffer, size_t BufferLength);

        private:
        //The error number
        int errorNumber;
        //The original message
        std::string originalMessage;

        //Composes the message
        static std::string composeMessage(const std::string & Message, int ErrorNumber, strerror_rXSIFunc TranslatingFunction);
        static std::string composeMessage(const std::string & Message, int ErrorNumber, strerror_rGNUFunc TranslatingFunction);
        static std::string composeMessage(const std::string & Message, int ErrorNumber, strerrorFunc TranslatingFunction);
        static std::string composeMessage(const std::string & Message, int ErrorNumber, const std::string & TranslatedErrorCode);

        public:

        //Init the class with the given ErrorNumber and translating function
        ErrnoError(const std::string & Message, int ErrorNumber
        #if !defined __USE_XOPEN2K || defined __USE_GNU
        =errno
        #endif
        , strerror_rGNUFunc TranslatingFunction
        #if !defined __USE_XOPEN2K || defined __USE_GNU
        =(strerror_rGNUFunc) strerror_r
        #endif
        );

        ErrnoError(const std::string & Message, int ErrorNumber
        #if defined __USE_XOPEN2K && !defined __USE_GNU
        =errno
        #endif
        , strerror_rXSIFunc TranslatingFunction
        #if defined __USE_XOPEN2K && !defined __USE_GNU
        =(strerror_rXSIFunc) strerror_r
        #endif
        );
        ErrnoError(const std::string & Message, int ErrorNumber, strerrorFunc TranslatingFunction);

        //Returns the error number
        int GetErrorNumber() { return errorNumber; };

        //Returns the original message
        const char * GetOriginalMessage() { return originalMessage.c_str(); };

        //Destructor
        virtual ~ErrnoError() throw() {};

    };
}
#endif // ERRNOERROR_HPP_INCLUDED
