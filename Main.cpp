//C++ headers
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <sstream>

//ALSA headers
#include <alsa/asoundlib.h>

//Project headers
#include "Main.hpp"
#include "ErrnoError.hpp"
#include "Joystick.hpp"

using namespace std;

int main(int argc, char * argv[])
{
    if(argc==1)
    {
        cout<<"JoyMIDI by Matteo Italia\n"
              "    Usage: JoyMIDI joystick channel\n"
              "\n"
              "        joystick    Path of the joystick device that will be used\n"
              "\n"
              "        channel     MIDI channel on which the messages will be sent;\n"
              "                    must be an integer in range 0-15.\n"
              "\n"
              "    Usage example:\n"
              "        JoyMIDI /dev/input/js0 0"<<endl;
        return 0;
    }
    try
    {
        if(argc<3)
            throw runtime_error("Not enough command line arguments.");
        istringstream is(argv[2]);
        int channel;
        is>>channel;
        if(!is.eof() || is.fail())
            throw invalid_argument("The channel argument must be an integer.");
        if(channel<0 || channel > 15)
            throw out_of_range("The channel argument must be in range 0-15.");
        StartJoystickMonitoring(argv[1], (unsigned int)channel);
    }
    catch(exception & ex)
    {
        cerr<<"\n---An exception occourred---\nException type: "<<typeid(ex).name()<<"\nMessage: "<<ex.what()<<endl;
        return 1;
    }
    return 0;
}

//Opens a new MIDI sequencer
snd_seq_t * OpenMIDISequencer()
{
    snd_seq_t * handle;
    int returnCode;
    returnCode = snd_seq_open(&handle, "default", SND_SEQ_OPEN_OUTPUT, 0);
    if (returnCode < 0)
        throw Utils::ErrnoError("Cannot create the MIDI port.", returnCode, snd_strerror);
    snd_seq_set_client_name(handle, "JoyMIDI");
    return handle;
}

//Creates a new MIDI readable port
int CreateMIDIReadablePort(snd_seq_t * Sequencer, const char * PortName)
{
        int returnCode=snd_seq_create_simple_port(Sequencer, PortName,
                        SND_SEQ_PORT_CAP_READ|SND_SEQ_PORT_CAP_SUBS_READ,
                        SND_SEQ_PORT_TYPE_MIDI_GENERIC | SND_SEQ_PORT_TYPE_SOFTWARE | SND_SEQ_PORT_TYPE_APPLICATION);
        if(returnCode<0)
            throw Utils::ErrnoError("Cannot create the MIDI port.", returnCode, snd_strerror);
        return returnCode;
}

//Sends an event on the given MIDI sequencer
void SendMIDIEvent(snd_seq_t * Sequencer, snd_seq_event_t & Event)
{
    int returnCode;
    if((returnCode=snd_seq_event_output(Sequencer, &Event))<0)
        throw  Utils::ErrnoError("Cannot send the MIDI event.", returnCode, snd_strerror);
}

//Flushes the MIDI events buffer
void FlushMIDIEventsBuffer(snd_seq_t * Sequencer)
{
    int returnCode;
    if((returnCode=snd_seq_drain_output(Sequencer))<0)
        throw Utils::ErrnoError("Cannot flush the MIDI events queue.", returnCode, snd_strerror);
}


//Starts the monitoring of the joystick
void StartJoystickMonitoring(const char * JoystickDevice, unsigned int Channel)
{
    //The default settings
    enum DefaultSettings
    {
        DefaultPitchValue=0,
        DefaultPitchSensitivityValue=2,
        DefaultModulationValue=0
    };
    //The axis->effect table
    enum AxesBindings
    {
        PitchAxis=0,
        PitchSensitivityAxis=2,
        ModulationAxis=1
    };
    //The button->effect table
    enum ButtonsBindings
    {
        ExitButton=0,
        ResetButton=3,
        EditPitchButton=1,
        EditPitchSensitivityButton=-1,
        EditModulationButton=2
    };
    //The joystick
    IO::Joystick joystick(JoystickDevice);
    //The sequencer
    snd_seq_t * sequencer=NULL;
    //The sequencer output port
    int seqOutputPort=-1;
    //Check if the joystick meets our requirements
    if(joystick.GetAxesNumber()<3)
        throw runtime_error("The joystick hasn't got enough axes.");
    if(joystick.GetButtonsNumber()<3)
        throw runtime_error("The joystick hasn't got enough buttons.");
    //Invert the axes 1 and 2
    joystick.GetInvertedAxes()[1]=true;
    joystick.GetInvertedAxes()[2]=true;
    //Display some info
    cout<<"Monitoring joystick "<<joystick.GetDeviceName().c_str()<<" ("<<joystick.GetAxesNumber()<<" axes, "<<joystick.GetButtonsNumber()<<" buttons)"<<endl;
    //Update the internal values
    joystick.UpdateValues();
    try
    {
        //Open the sequencer
        sequencer=OpenMIDISequencer();
        //Create the port
        seqOutputPort=CreateMIDIReadablePort(sequencer, "JoyMIDI output");
        //Used in the loop to avoid sending unnecessary messages
        int lastPitchValue=DefaultPitchValue;
        int lastPitchSensitivityValue=DefaultPitchSensitivityValue;
        int lastModulationValue=DefaultModulationValue;
        //Set if the exit button was down in the last iteration, set if we have to exit, set if we have to reset the controls
        bool exitButtonDown=false, exit=false, reset=false;
        //The MIDI event
        snd_seq_event_t event;
        //Set up its constant properties
        snd_seq_ev_set_source(&event, seqOutputPort);
        snd_seq_ev_set_subs(&event);
        snd_seq_ev_set_direct(&event);
        //Main cycle
        for(;;)
        {
            //The controlled values
            int pitchValue=lastPitchValue, pitchSensitivityValue=lastPitchSensitivityValue, modulationValue=lastModulationValue;
            //If the exit button has been pressed and released exit
            exit=exitButtonDown&&!joystick.GetLastButtonsValues()[ExitButton];
            //Update the state of the exitButtonDown flag
            exitButtonDown=joystick.GetLastButtonsValues()[ExitButton];
            //Update the state of the reset flag
            reset=joystick.GetLastButtonsValues()[ResetButton];
            //We are exiting or the reset button has been pressed; reset the values
            if(exit || reset)
            {
                pitchValue=DefaultPitchValue;
                pitchSensitivityValue=DefaultPitchSensitivityValue;
                modulationValue=DefaultModulationValue;
            }
            //Update the values
            else
            {
                //Pitch
                if(EditPitchButton<0 || joystick.GetLastButtonsValues()[EditPitchButton])
                    pitchValue=joystick.GetLastAxesValues()[PitchAxis]/4;
                //Pitch sensitivity
                if(EditPitchSensitivityButton<0 || joystick.GetLastButtonsValues()[EditPitchSensitivityButton])
                    pitchSensitivityValue=(int)((joystick.GetLastAxesValues()[PitchSensitivityAxis]+32767.0)*24.0/65534.0);
                //Modulation
                if(EditModulationButton<0 || joystick.GetLastButtonsValues()[EditModulationButton])
                    modulationValue=(int)((joystick.GetLastAxesValues()[ModulationAxis]+32767.0)*127.0/65534.0);
            }
            //Check to avoid unnecessary messages and actual message send
            //Pitch value
            if(pitchValue!=lastPitchValue)
            {
                //Prepare the message
                snd_seq_ev_set_pitchbend(&event,Channel,pitchValue);
                //Send the message
                SendMIDIEvent(sequencer, event);
                //Flush the events buffer
                FlushMIDIEventsBuffer(sequencer);
                cout<<"Sent pitch bend "<<pitchValue<<" on channel "<<Channel<<"."<<endl;
                //If we are resetting don't store the changed value, so the resetted value stays until the value changes
                if(!reset)
                    lastPitchValue=pitchValue;
            }
            //Pitch sensitivity value
            if(pitchSensitivityValue!=lastPitchSensitivityValue)
            {
                //1. RPN LSB
                snd_seq_ev_set_controller(&event, Channel, 0x64, 0x00);
                SendMIDIEvent(sequencer, event);
                //2. RPN MSB
                snd_seq_ev_set_controller(&event, Channel, 0x65, 0x00);
                SendMIDIEvent(sequencer, event);
                //3. RPN data entry MSB
                snd_seq_ev_set_controller(&event, Channel, 0x06, pitchSensitivityValue);
                SendMIDIEvent(sequencer, event);
                //4. RPN data entry LSB
                snd_seq_ev_set_controller(&event, Channel, 0x26, 0x00);
                SendMIDIEvent(sequencer, event);
                //5. RPN NULL LSB
                snd_seq_ev_set_controller(&event, Channel, 0x64, 0x7F);
                SendMIDIEvent(sequencer, event);
                //6. RPN NULL MSB
                snd_seq_ev_set_controller(&event, Channel, 0x65, 0x7F);
                SendMIDIEvent(sequencer, event);
                //Flush the events buffer
                FlushMIDIEventsBuffer(sequencer);
                cout<<"Sent pitch bend sensitivity change "<<pitchSensitivityValue<<" on channel "<<Channel<<"."<<endl;
                //If we are resetting don't store the changed value, so the resetted value stays until the value changes
                if(!reset)
                    lastPitchSensitivityValue=pitchSensitivityValue;
            }
            //Modulation value
            if(modulationValue!=lastModulationValue)
            {
                //Actual data
                snd_seq_ev_set_controller(&event, Channel, 0x01, modulationValue);
                //Send the message
                SendMIDIEvent(sequencer, event);
                //Flush the events buffer
                FlushMIDIEventsBuffer(sequencer);
                cout<<"Sent modulation change "<<modulationValue<<" on channel "<<Channel<<"."<<endl;
                //If we are resetting don't store the changed value, so the resetted value stays until the value changes
                if(!reset)
                    lastModulationValue=modulationValue;
            }
            //Exit if requested
            if(exit)
                break;
            //Acquire a new event in blocking mode
            joystick.GetEvent();
        }
        cout<<"Exit button pressed, exiting..."<<endl;
        //Cleanup
        snd_seq_delete_simple_port(sequencer, seqOutputPort);
        snd_seq_close(sequencer);
    }
    catch(...)
    {
        //Cleanup
        if(seqOutputPort>=0 && sequencer!=NULL)
            snd_seq_delete_simple_port(sequencer, seqOutputPort);
        if(sequencer != NULL)
            snd_seq_close(sequencer);
        //Rethrow
        throw;
    }
}
