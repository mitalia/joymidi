#include <sstream>
#include <string>

#include "ErrnoError.hpp"
#include "Utils.hpp"

Utils::ErrnoError::ErrnoError(const std::string & Message, int ErrorNumber, Utils::ErrnoError::strerror_rGNUFunc TranslatingFunction) :
    std::runtime_error(composeMessage(Message, ErrorNumber, TranslatingFunction)),
    errorNumber(errorNumber),
    originalMessage(Message)
{

}

Utils::ErrnoError::ErrnoError(const std::string & Message, int ErrorNumber, Utils::ErrnoError::strerror_rXSIFunc TranslatingFunction) :
    std::runtime_error(composeMessage(Message, ErrorNumber, TranslatingFunction)),
    errorNumber(errorNumber),
    originalMessage(Message)
{

}

Utils::ErrnoError::ErrnoError(const std::string & Message, int ErrorNumber, Utils::ErrnoError::strerrorFunc TranslatingFunction) :
    std::runtime_error(composeMessage(Message, ErrorNumber, TranslatingFunction)),
    errorNumber(errorNumber),
    originalMessage(Message)
{

}

std::string Utils::ErrnoError::composeMessage(const std::string & Message, int ErrorNumber, const std::string & TranslatedErrorCode)
{
    std::ostringstream os;
    os<<Message.c_str()<<" - Error "<<ErrorNumber<<" ("<<TranslatedErrorCode.c_str()<<").";
    return os.str();
}


std::string Utils::ErrnoError::composeMessage(const std::string & Message, int ErrorNumber, Utils::ErrnoError::strerrorFunc TranslatingFunction)
{
    return composeMessage(Message, ErrorNumber, (char *)TranslatingFunction(ErrorNumber));
}

std::string Utils::ErrnoError::composeMessage(const std::string & Message, int ErrorNumber, Utils::ErrnoError::strerror_rXSIFunc TranslatingFunction)
{
    char buffer[1024];
    *buffer=0;
    const char * bufPtr=buffer;
    errno=0;
    if(TranslatingFunction(ErrorNumber, buffer, ARRSIZE(buffer))!=0)
    {
        switch(errno)
        {
            case EINVAL:
                bufPtr="no error description string available, invalid error number";
                break;
            case ERANGE:
                bufPtr="no error description string available, not enough buffer space";
                break;
            default:
                bufPtr="no error description string available, the translating function failed for an unknown reason";
        }
    }
    return composeMessage(Message, ErrorNumber, bufPtr);
}

std::string Utils::ErrnoError::composeMessage(const std::string & Message, int ErrorNumber, Utils::ErrnoError::strerror_rGNUFunc TranslatingFunction)
{
    char buffer[1024];
    *buffer=0;
    const char * bufPtr=buffer;
    bufPtr=TranslatingFunction(ErrorNumber, buffer, ARRSIZE(buffer));
    return composeMessage(Message, ErrorNumber, bufPtr);
}
